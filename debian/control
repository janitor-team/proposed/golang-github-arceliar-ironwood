Source: golang-github-arceliar-ironwood
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: John Goerzen <jgoerzen@complete.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-go (>= 2:1.17~0),
               golang-github-arceliar-phony-dev,
               golang-golang-x-crypto-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-arceliar-ironwood
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-arceliar-ironwood.git
Homepage: https://github.com/Arceliar/ironwood
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/Arceliar/ironwood

Package: golang-github-arceliar-ironwood-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-arceliar-phony-dev,
         golang-golang-x-crypto-dev,
         ${misc:Depends}
Description: Routing library with public keys as addresses (library)
 Ironwood is a routing library with a net.PacketConn-compatible interface
 using ed25519.PublicKeys as addresses. Basically, you use it when you
 want to communicate with some other nodes in a network, but you can't
 guarantee that you can directly connect to every node in that network.
 It was written to test improvements to / replace the routing logic in
 Yggdrasil (https://github.com/yggdrasil-network/yggdrasil-go), but it may
 be useful for other network applications.
 .
 Note: Ironwood is pre-alpha work-in-progress. There's no stable API,
 versioning, or expectation that any two commits will be compatible with
 each other. Also, it hasn't been audited by a security expert. While the
 author is unaware of any security vulnerabilities, it would be wise to
 think of this as an insecure proof-of-concept. Use it at your own risk.

